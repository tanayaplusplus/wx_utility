from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg
import matplotlib.cbook as cbook

#--------------------------------------------------------------------------------------------------
class Eight_bit_signal_toolbar(NavigationToolbar2WxAgg):
    def __init__(self, canvas, status_bar):
        NavigationToolbar2WxAgg.__init__(self, canvas)
        self._status_bar = status_bar

    def mouse_move(self, event):
        self._update_cursor(event)

        if event.inaxes and event.inaxes.get_navigate():

            try:
                s = f'x={int(event.xdata + 0.5)}, y={event.ydata}'
            except (ValueError, OverflowError):
                pass
            else:
                artists = [a for a in event.inaxes._mouseover_set
                           if a.contains(event)[0] and a.get_visible()]

                if artists:
                    a = cbook._topmost_artist(artists)
                    if a is not event.inaxes.patch:
                        data = a.get_cursor_data(event)
                        if data is not None:
                            s += ' [%s]' % a.format_cursor_data(data) # [0x%08X]' % data

                if len(self.mode):
                    self._status_bar.SetStatusText('%s, %s' % (self.mode, s))
                else:
                    self._status_bar.SetStatusText(s)
        else:
            self._status_bar.SetStatusText(self.mode)
