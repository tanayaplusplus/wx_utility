# Module dialog_config
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

import wx
import os
from .dialog_element import DlgItemValidator
from .dialog_element import Dialog_element

def config_value(do_read, config, section_name, key, value):
    """
    Read a bool/float/int/string from a configparser. (Write is trivial)

    :do_read       True for read; False for write
    :config        configparser instance.
    :section_name  Section name string.
    :key           Key string.
    :value         When writing (do_read is False), contains the value written
                   to the configparser.
                   When reading, is the default value, returned if the item is
                   not present in the configparser instance.

    Returns value unchanged when do_read is False. For reading, returns the
    value extracted from the configparser if the item is present, or returns
    value unchanged.
    """
    if do_read:
        if config.has_option(section_name, key):
            if isinstance(value, bool):
                try:
                    value = config.getboolean(section_name, key)
                    # logger.debug('valu is %s', value)
                except AttributeError:
                    s = config.get(section_name, key)
                    # logger.debug('config.get() "%s", type(s) %s', s, type(s))
                    if isinstance(s, bool):
                        value = s
                        # logger.debug('setting value %s', value)
            elif isinstance(value, float):
                value = config.getfloat(section_name, key)
            elif isinstance(value, int):
                value = config.getint(section_name, key)
            else:
                # Treat as a string.
                value = config.get(section_name, key)
    else:
        if not config.has_section(section_name):
            config.add_section(section_name)
        config.set(section_name, key, value)
    return value


# -------------------------------------------------------------------------------------------------
def config_read(config, section, param_dict):
    """
    Read values from configparser into a dictionary of Dialog_elements.

    :config     configparser instance.
    :section    Section heading in configuration file to be read from. If the section is not
                present in the configuration, then this function does nothing.
    :param_dict Dictionary with Dialog_elements instances which are to be modified by values in config.
                If an item is not present in the configuration file, then its dictionary entry is
                not modified.
    """
    if config.has_section(section):
        for key in param_dict:
            itm = param_dict[key]
            # config_value() will return the current value, unchanged, if
            # the option is not present in the configuration file.
            itm.value = config_value(True, config, section, key, itm.value)


# -------------------------------------------------------------------------------------------------
def config_save(config, section, param_dict):
    """
    Saves the values in a dictionary of Dialog_elements to configparser.

    :config     configparser instance.
    :section    Section heading in configuration file to be written to. If the section is not
                present in the configuration, then it is added.
    :param_dict Dictionary with Dialog_element instances whose values are to be written to config.
                If an item is not present in the configuration file, then it is added.
    """
    if not config.has_section(section):
        config.add_section(section)

    for key in param_dict:
        config.set(section, key, param_dict[key].value)

# -----------------------------------------------------------------------------
def transfer_vars_from_ui(parent_panel):
    '''
    Validate controls and transfer data to associated dictionary items.

    :param wx.Panel parent_panel: Panel (or any wx.Window) associated with the 
                    dialog items to validate
    :return:    True if panel values validated and transferred correctly
    '''
    return parent_panel.Validate() and parent_panel.TransferDataFromWindow()


# -----------------------------------------------------------------------------
def box_controls_and_labels(parent, box_title, dlg_items, ctrl_width=60,
                            extra_gap=15, item_range=None,
                            browse_handler=None, browse_dict=None,
                            ctrl_list=None):
    '''
    Returns a StaticBoxSizer with aligned labels and controls.

    Lays out a row of controls and static text for each dialog_element instance in
    dlg_items.
    If the dialog_element requires a checkbox (the dialog_element stores a bool value), then
    the row contains a single wx.CheckBox that spans all columns in the row.
    If the dialog_element requires a text control (the dialog_element stores a non-bool
    value), then the row comprises two or three columns. The first two columns
    are a label (prompt string), and the edit control. A unit string or a
    Browse button may be present in a third column.
    The column of unit strings/Browse buttons is only added when at least one
    dialog_element requires a unit string or Browse button.

    :param parent:         Parent window of the controls.
    :param box_title:      Title for the static box surrounding the labels and
                            controls.
    :param dlg_items:      Ordered dictionary of dialog_element instances, in the order they
                            should appear down the column.
    :param ctrl_width:     Width of the text edit controls (60 is usually sufficient
                            for numerical values).
    :param extra_gap:      Extra space added below each row, and to the left of the
                            labels and to the right of the controls.
    :param item_range      None - use all the items in dlg_items; or
                            Tuple of first and last keys to restrict the items
                            displayed; or
                            String corresponding to key of single item to display.
    :param browse_handler  Function to be called when a Browse button is clicked.
    :param browse_dict:    
    :param ctrl_list:      If this parameter is a list, then the list of text controls
                            is added to ctrl_list with extend().
                            This parameter is typically used when a group of controls
                            will be grayed etc. (possibly using enable_window_list())
                            in response to other dialog events.
    '''
    use_gbs = True  # For all items displayed, use grid bag sizer now.

    sbox_szr = wx.StaticBoxSizer(wx.VERTICAL, parent, label=box_title)

    if use_gbs:
        bgs = add_controls_and_text(parent, dlg_items, ctrl_width, extra_gap,
                                    item_range, browse_handler, browse_dict,
                                    ctrl_list)
        # Add the sizer with the controls to the box sizer, with a gap of ~10
        # above and below the controls' sizer.
        sbox_szr.Add(bgs, 0, wx.TOP | wx.BOTTOM, (2 * extra_gap) // 3)
    return sbox_szr


#TODO: Rewrite!
# -----------------------------------------------------------------------------
def add_controls_and_text(parent, dlg_items, ctrl_width=60, extra_gap=15,
                          item_range=None, browse_handler=None,
                          browse_dict=None, ctrl_list=None):
    """Returns a wx.GridBagSizer with aligned labels and controls.

    Lays out a row of controls and static text for each dialog_element instance in
    dlg_items.
    If the dialog_element requires a checkbox (the dialog_element stores a bool value), then
    the row contains a single wx.CheckBox that spans all columns in the row.
    If the dialog_element requires a text control (the dialog_element stores a non-bool
    value), then the row comprises two or three columns. The first two columns
    are a label (prompt string), and the edit control. A unit string or a
    Browse button may be present in a third column.
    The column of unit strings/Browse buttons is only added when at least one
    dialog_element requires a unit string or Browse button.
    dlg_items       Ordered dictionary of dialog_element instances, in the order they
                    should appear down the rows.
    ctrl_width      Width of the text edit controls (60 is usually sufficient
                    for numerical values).
    extra_gap       Extra space added to the left of the leftmost control, and
                    to the right of the rightmost control, in each row.
    item_range      None - use all the items in dlg_items; or
                    Tuple of first and last keys to restrict the items
                    displayed; or
                    String corresponding to key of single item to display.
    browse_handler  Function to be called when a Browse button is clicked.
    browse_dict
    ctrl_list       If this parameter is a list, then the list of text controls
                    is added to ctrl_list with extend().
                    This parameter is typically used when a group of controls
                    will be grayed etc (possibly using enable_window_list()) in
                    response to other dialog events).
    """
    #@@@ Option to capitalize first letter of dictionary key for prompt.
    labels = list()     # List of static text controls.
    ctrls = list()      # List of wx.TextCtrl or wx.CheckBox items.
    units = list()      # List of static text controls.
    browsers = list()   # List of Browse buttons.
    ctrl_size = (ctrl_width, -1)  # Use default text control height.
    max_unit_chars = 0

    # If the caller provided a single string for the item range, then make a
    # tuple with two values equal to the string.
    if isinstance(item_range, str):
        item_range = (item_range, item_range)
    # Set a flag for whether the current dictionary entry is within the
    # required range; it always is if we're using the full range.
    in_range = (item_range is None)

    for key, item in dlg_items.items():
        # Check to see if we are within the specified range.
        if not in_range:
            in_range = (key == item_range[0])

        if in_range:
            if item.is_bool:
                labels.append(-1)  # Use integer to distinguish from empty string.
                ctrls.append(wx.CheckBox(parent, wx.ID_ANY, item.label,
                                         validator=DlgItemValidator(item)))
                units.append(None)
                browsers.append(None)
            else:
                # Use the label in the dialog item, if one was provided.
                # Otherwise, use the dictionary key as the prompt label.
                label = item.label
                if len(label) < 1:
                    label = key
                labels.append(wx.StaticText(parent, -1, label))
                sz = (ctrl_size if (item.control_width == 0) else
                      (item.control_width, -1))
                if item.style == item.Spin_control:
                    sc = wx.SpinCtrlDouble(parent, 
                                                size=sz, 
                                                min=item.min_value, 
                                                max=item.max_value,
                                                inc=item.increment)
                    sc.SetValidator(DlgItemValidator(item))
                    ctrls.append(sc)
                else:
                    ctrls.append(wx.TextCtrl(parent, size=sz,
                                            validator=DlgItemValidator(item)))
                s = item.unit_str
                max_unit_chars = max(max_unit_chars, len(s))
                units.append(wx.StaticText(parent, -1, s))
                if item.style in [Dialog_element.Browse_file_open, 
                                  Dialog_element.Browse_file_save, 
                                  Dialog_element.Browse_directory]:
                    b = wx.Button(parent, wx.ID_ANY, 'Browse')
                    browse_dict[b.GetId()] = key
                    browsers.append(b)
                    parent.Bind(wx.EVT_BUTTON, browse_handler, b)
                else:
                    browsers.append(None)
            # If we just handled the last item in the range, get out of loop.
            if (item_range is not None) and (key == item_range[1]):
                break

    # Each label, text control and Button or unit string is left-aligned in its
    # column, and centered vertically in the row.
    alignment = wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL

    # The label is drawn with an additional gap to the left of the leftmost
    # column, using wx.LEFT
    label_flag = alignment | wx.LEFT

    # The rightmost column (button/text/control) has an additional gap on the
    # right side, using wx.RIGHT
    rightmost_flag = alignment | wx.RIGHT

    # The checkboxes are left-aligned, and have additional gap on left, right
    # and bottom.
    check_flag = wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT # | wx.BOTTOM

    # Add a little extra vertical space when the control type changes.
    vertical_gap = (5, (2 * extra_gap) // 3)

    # Create the grid bag sizer.
    gbs = wx.GridBagSizer(hgap=10, vgap=10)

    # Only add a third column if we need it (ignored for checkboxes).
    n_cols = 3 if ((max_unit_chars >= 1) or (any(browsers))) else 2
    # If the text control is the rightmost one, then add the extra gap on its
    # right side.
    textctrl_flag = rightmost_flag if (n_cols < 3) else alignment

    row = 0
    last_type = 'Nothing'

    for i, label in enumerate(labels):
        if isinstance(label, int):
            # Add a vertical space if the last row had a text control.
            # The span associated with vertical gap may not be necessary, but
            # has little downside.
            if last_type == 'textctrl':
                pass
                # gbs.Add(vertical_gap, pos=(row, 0), span=(1, n_cols))
                # row += 1
            last_type = 'checkbox'
            # Add the checkbox. span is a tuple of the number of rows and
            # number of columns to be spanned by the control.
            gbs.Add(ctrls[i], pos=(row, 0), span=(1, n_cols),
                    flag=check_flag, border=extra_gap)
        else:
            # Add a vertical space if the last row had a checkbox.
            # The span associated with vertical gap may not be necessary, but
            # has little downside.
            if last_type == 'checkbox':
                pass
                # gbs.Add(vertical_gap, pos=(row, 0), span=(1, n_cols))
                # row += 1
            last_type = 'textctrl'
            gbs.Add(label, pos=(row, 0), flag=label_flag, border=extra_gap)
            gbs.Add(ctrls[i], pos=(row, 1), flag=textctrl_flag,
                    border=extra_gap)
            if n_cols == 3:
                if browsers[i] is not None:
                    gbs.Add(browsers[i], pos=(row, 2), flag=rightmost_flag,
                            border=extra_gap)
                else:
                    gbs.Add(units[i], pos=(row, 2), flag=rightmost_flag,
                            border=extra_gap)
        row += 1

    # If the caller provided a list parameter, add the ctrls list to it.
    if isinstance(ctrl_list, list):
        ctrl_list.extend(ctrls)

    return gbs

#TODO: rewrite!
# -------------------------------------------------------------------------------------------------
def create_okay_and_cancel(parent, draw_side_by_side=True, labels=None):
    '''
    Creates OK and Cancel buttons for a dialog.

    :param parent:             Parent window of the buttons.
    :param draw_side_by_side:  True to draw buttons side by side
                                False to draw one button above the the other.
                                (Ordering of the two buttons is OS-dependent.)
    :param labels:             The first label in a non-empty list will replace the
                                standard "OK" label; the second label will replace
                                the standard "Cancel" label.
    :return:           A tuple of the StdDialogButtonSizer, and the size of a single button.
    '''
    btns = wx.StdDialogButtonSizer()
    btns.SetOrientation(wx.HORIZONTAL if draw_side_by_side else wx.VERTICAL)
    for i, wx_id in enumerate([wx.ID_OK, wx.ID_CANCEL]):
        b = wx.Button(parent, wx_id)
        if wx_id == wx.ID_OK:
            b.SetDefault()
        if (labels is not None) and (len(labels) > i):
            b.SetLabel(labels[i])
        btns.AddButton(b)

    btns.Realize()
    return btns, b.GetSize()

# -------------------------------------------------------------------------------------------------
def browse_and_update(event, dlg_item_dict, button_dict, update_function):
    '''
    Browse for file or directory, and update dialog_element instance.

    This function is typically called from a dialog's event handler function
    that is bound to Browse button clicks.

    :param event:            The event passed to the handler for the button click.
    :param dlg_item_dict:    Dictionary of dialog items associated with the buttons.
    :param button_dict:      Dictionary with keys that are button IDs, and values that
                                are keys into dlg_item_dict. Typically, this dictionary is
                                populated by passing the empty dictionary to
                                box_controls_and_labels() or add_controls_and_labels().
    :param update_function:  Callback function invoked to update the text controls associated
                                with the dialog_element dictionary. Typically, this is the
                                wx.Window function self.TransferDataToWindow.
                                Note: Provide this parameter without trailing parentheses.
    '''
    dialog_item = dlg_item_dict[button_dict[event.GetId()]]
    s = dialog_item.style
    if s == Dialog_element.Browse_directory:
        dialog_title = 'Select directory'
        dlg = wx.DirDialog(None, dialog_title, dialog_item.value)
        if dlg.ShowModal() == wx.ID_OK:
            dialog_item.value = dlg.GetPath()
        dlg.Destroy()
    else:                    # Browse_file_open = 1 or Browse_file_save = 2
        dialog_title = 'Select file'
        def_dir, def_fname = '', ''
        if len(dialog_item.value) > 0:
            def_dir, def_fname = os.path.split(dialog_item.value)
        file_save = (s == Dialog_element.Browse_file_save)
        dialog_style = (wx.FD_SAVE if (file_save) else wx.FD_OPEN)
        dlg = wx.FileDialog(None, dialog_title, def_dir, def_fname,
                            style=dialog_style)
        if dlg.ShowModal() == wx.ID_OK:
            # Retrieve the full path.
            dialog_item.value = os.path.join(dlg.GetDirectory(), dlg.GetFilename())
        dlg.Destroy()
    # Now update the text controls.
    update_function()
