# Module app_config
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

import argparse
import logging
import wx
import sys
import os

def pyinst_resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    """ http://stackoverflow.com/questions/7674790/bundling-data-files-with-pyinstaller-onefile """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


def initialize_global_parser(project_name):
    """Construct parser with arguments, for parsing command line."""

    p = argparse.ArgumentParser(description='PCO camera demo app')
    p.add_argument('cfg_file', type=str, default=f'{project_name}.ini',
                   help='Specify a configuration (.cfg or .ini) file.')
    p.add_argument('-l', '--loglevel', type=str, 
                   help='Choose from options CRITICAL, ERROR, WARNING, INFO, or DEBUG.')
    return p


def run_wx(parser, app_class, project_name=None):
    # Parse the command line arguments. If there's a problem, exit immmediately
    # with exit status 2.
    args = parser.parse_args()

    if os.path.exists(f'{project_name}.log'):
        os.remove(f'{project_name}.log')
    try:
        logging_level = logging.CRITICAL
        if args.loglevel == 'DEBUG':
            logging_level = logging.DEBUG
        elif args.loglevel == 'ERROR':
            logging_level = logging.ERROR
        elif args.loglevel == 'WARNING':
            logging_level = logging.WARNING
        elif args.loglevel == 'INFO':
            logging_level = logging.INFO
        logging.basicConfig(level=logging_level, 
                            format='%(asctime)s: [%(levelname)s]  %(message)s',
                            filename=f'{project_name}.log')
    except: #what?
        pass

    if len(args.cfg_file) < 2:
        sys.exit('\nYou must provide a configuration file name on the command line.')

    app = app_class(**vars(args))
    app.MainLoop()
