# Module dialog_element
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

class Dialog_element:

    Browse_nothing = 0
    Browse_file_open = 1
    Browse_file_save = 2
    Browse_directory = 4
    Spin_control = 5

    # ---------------------------------------------------------------------------------------------
    def __init__(self, value, min_value=0, max_value=0, label='', unit_str='',
                 is_hex=False, mult_of=1, increment=None, style=0, control_width=0):
        self._value = value
        self.min_value = min_value
        self.max_value = max_value
        self.label = label
        self.unit_str = unit_str
        self._is_hex = is_hex
        self.multiple_of = int(mult_of if (mult_of != 0) else 1)
        self.increment = increment
        self.style = Dialog_element.Spin_control if self.increment else style 
        self.control_width = control_width

    # ---------------------------------------------------------------------------------------------
    def __str__(self):
        return format(self.value, 'X') if (self._is_hex) else str(self.value)

    # ---------------------------------------------------------------------------------------------
    @property
    def is_bool(self):
        """Return True if value is type bool, otherwise False."""
        # This is a common test made by Dialog_element-containing dialogs.
        return isinstance(self._value, bool)

    # ---------------------------------------------------------------------------------------------
    @property
    def radix(self):
        """Returns 16 for hex values; 10 for everything else."""
        # The radix only has meaning for int or hex types, of course.
        return 16 if (self._is_hex) else 10

    # ---------------------------------------------------------------------------------------------
    @property
    def value(self):
        """Returns the current data value."""
        return self._value

    # ---------------------------------------------------------------------------------------------
    @value.setter
    def value(self, new_value):
        """Sets the current data value."""
        # This code forces the type of self._value to remain one of bool, int, float or str.
        # bool test must come before int because bool "is a" int.
        if self.is_bool:
            self._value = bool(new_value)
        elif isinstance(self._value, int):
            self._value = (int(new_value, self.radix)
                           if isinstance(new_value, str) else int(new_value))
        elif isinstance(self._value, float):
            self._value = float(new_value)
        else:
            self._value = str(new_value)

        self._enforce_mult_of()
        return

    # ---------------------------------------------------------------------------------------------
    def _enforce_mult_of(self):
        # Be careful here: isinstance(x, int) will return True if x is bool OR int.
        # Exclude bool first, otherwise bool gets converted to int here.
        if (not self.is_bool) and (isinstance(self._value, int)):
            m = self.multiple_of
            self._value = (self._value // m) * m
        return


#TODO
#
    # ---------------------------------------------------------------------------------------------
    def check_text(self, text):
        """Checks whether the provided text is valid and could be converted and saved in this
        DlgItem instance.

        The internal value is NOT set by this function - this is only a test.
        Returns an empty string if the text could be interpreted as a valid value; otherwise a
        non-empty string explaining the failure.

        This function is used by DlgItemValidator for validating values entered in a wx.TextCtrl.
        """
        # So far, we're not doing anything for bool values here.
        msg = ''
        if self.is_bool:
            pass
        elif isinstance(self._value, float):
            msg = self._check_float(text)
        elif isinstance(self._value, int):
            msg = self._check_integer(text)
        else:      # assume str for anything else.
            msg = self._check_string(text)
        return msg

    # ---------------------------------------------------------------------------------------------
    def _check_limits(self):
        """Returns True if range or length checking is required as part of data validation."""
        # We do no range checking if both values are 0.
        return True if ((self.min_value != 0) or
                        (self.max_value != 0)) else False

    # ---------------------------------------------------------------------------------------------
    def _check_float(self, text):
        """Performs additional validation for float data type."""
        # Return empty string if there's no problem.
        msg = ''
        # First check that text can be interpreted as a float.
        try:
            val = float(text)
        except ValueError:
            msg = self.label + ': ' + text + ' is not a valid floating point number.'

        if ((len(msg) == 0) and (self._check_limits()) and
                ((val < self.min_value) or (val > self.max_value))):
            msg = (self.label + ' must be in the range [' +
                   str(self.min_value) + ', ' + str(self.max_value) + ']')
        return msg

    # ---------------------------------------------------------------------------------------------
    def _check_integer(self, text):
        """Performs additional validation for integer data type."""
        # Return empty string if there's no problem.
        msg = ''
        # First check that text can be interpreted as an integer.
        try:
            val = int(text, self.radix)
        except ValueError:
            msg = self.label + ': ' + text + ' is not a valid '
            if self.radix == 16:
                msg += '(hex) '
            msg += 'integer.'

        if ((len(msg) == 0) and (self._check_limits()) and
                ((val < self.min_value) or (val > self.max_value))):
            msg = (self.label + ' must be in the range [' +
                   str(self.min_value) + ', ' + str(self.max_value) + ']')

        m = self.multiple_of  # Can never be 0; usually is 1.
        #@@@ Review negative values of m!!
        if (len(msg) == 0) and ((val // m) * m) != val:
            msg = self.label + ' must be a multiple of ' + str(m) + '.'
        return msg

    # ---------------------------------------------------------------------------------------------
    def _check_string(self, text):
        """Performs additional validation for string data type."""
        # Return empty string if there's no problem.
        msg = ''
        # We do no length checking if both limits are 0.
        if self._check_limits():
            min_length = self.min_value
            max_length = self.max_value
            if ((len(text) < min_length) or
                    ((max_length > 0) and (len(text) > max_length))):
                msg = self.label + ' must have '
                if max_length == 0:
                    msg += 'at least ' + str(min_length)
                else:
                    msg += str(min_length) + ' to ' + str(max_length)
                msg += ' characters.'
        return msg


import wx
import wx.adv as addd
# The code is based on the section on Validators in "wxPython in Action",
# pages 282-291.

# =================================================================================================
class DlgItemValidator(wx.Validator):
    """Validator for DlgItem instances."""

    # -------------------------------------------------------------------------
    def __init__(self, dlg_element):
        """Constructor taking a Dialog_element instance."""
        super(DlgItemValidator, self).__init__()
        self._dlg_elem = dlg_element
        return

    # -------------------------------------------------------------------------
    def Clone(self):
        """Every validator must implement the Clone() method."""
        return DlgItemValidator(self._dlg_elem)

    # -------------------------------------------------------------------------
    def Validate(self, parent_window):   # pylint: disable=unused-argument
        """Validation function, with checks depending on DlgItem data type."""
        # We are not using the parent_window argument here.
        ctrl = self.GetWindow()
        ret = True
        # For a bool (used in checkbox), no additional validation or text
        # display is required.
        if not self._dlg_elem.is_bool:
            # Check the text from the edit control.
            msg = self._dlg_elem.check_text(ctrl.GetValue())
            if len(msg) == 0:           # No problem with the text?
                # Reset any possibly-changed background color.
                ctrl.SetBackgroundColour(
                    wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
            else:
                #TODO: Dialog_element object has no attribute "validation_title"
                wx.MessageBox(msg, self._dlg_elem.validation_title)
                ctrl.SetBackgroundColour('pink')
                ctrl.SetFocus()
                ret = False
        ctrl.Refresh()
        return ret

    # -------------------------------------------------------------------------
    def TransferToWindow(self):
        """Populates edit control or checkbox from DlgItem value."""
        # For a bool data type (associated with a checkbox), use the DlgItem's
        # value directly. For edit controls, we use the string representation.
        self.GetWindow().SetValue(self._dlg_elem.value
                                  if (self._dlg_elem.is_bool) else
                                  str(self._dlg_elem))
        return True

    # -------------------------------------------------------------------------
    def TransferFromWindow(self):
        """Sets DlgItem value from contents of edit control or checkbox."""
        self._dlg_elem.value = self.GetWindow().GetValue()
        return True


# =================================================================================================
class DlgItemHexValidator(DlgItemValidator):
    """Validator for DlgItem instances."""

    # -------------------------------------------------------------------------
    def __init__(self, dlg_item):
        """Constructor taking a DlgItem instance."""
        super(DlgItemHexValidator, self).__init__(dlg_item)
        return

    # -------------------------------------------------------------------------
    def Clone(self):
        """Every validator must implement the Clone() method."""
        return DlgItemHexValidator(self._dlg_elem)

    # -------------------------------------------------------------------------
    def TransferToWindow(self):
        """Populates edit control or checkbox from DlgItem value."""
        # Only for an int data type. For edit controls, we use the string 
        # representation formatted with padding to 8 digits
        self.GetWindow().SetValue('{:08X}'.format(self._dlg_elem.value))
        return True


# =================================================================================================
class DlgItemComboValidator(DlgItemValidator):
    """Validator for DlgItem instances."""

    # -------------------------------------------------------------------------
    def __init__(self, dlg_item):
        """Constructor taking a DlgItem instance."""
        super(DlgItemComboValidator, self).__init__(dlg_item)
        return

    # -------------------------------------------------------------------------
    def Clone(self):
        """Every validator must implement the Clone() method."""
        return DlgItemComboValidator(self._dlg_elem)

    # -------------------------------------------------------------------------
    def Validate(self, parent_window):   # pylint: disable=unused-argument
        return True
