# Module frame_config
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

import wx
import configparser
from .dialog_config import (config_value, transfer_vars_from_ui)

class Base_wx_frame(wx.Frame):

    def __init__(self, frame_title, cfg_filename):
        ''' '''
        if not isinstance(cfg_filename, str) or len(cfg_filename) < 2:
            raise RuntimeError('Invalid cfg_filename value was passed'
                               ' to the Base_wx_frame constructor. Name must be'
                               ' string type longer than 2 characters.')
        self._cfg_filename = cfg_filename

        self._cfg_parser = configparser.RawConfigParser()
        self._cfg_parser.optionxform = str # Retain case of option names.

        self._load_configuration_file()
        self._cfg_section_name = 'Mainframe'

        # Get the frame position and size before initializing the base frame.
        # self._cfg_section_name needs to be a member variable, otherwise the File | Exit option
        # hangs; don't just pass 'Mainframe' as explicit last argument to get_frame_layout().
        # TODO Should get_frame_layout() make a copy, to avoid this problem?
        # (Using a static function here, but could just use the instance?)
        self._frame_layout = {'left' : 100, 
                            'top' : 100, 
                            'width' : 800, 
                            'height' : 600, 
                            'minimized' : False}

        was_min, posn, sz = self.get_frame_layout()
        super(Base_wx_frame, self).__init__(None, wx.ID_ANY, frame_title, posn, sz)

        self.Bind(wx.EVT_CLOSE, self.OnClose)

        # TODO
        #self.Bind(wx.EVT_ICONIZE, self.Iconizing)

        self._control_area = None
        self._control_pages = list()
        self._data_area = None
        self._data_pages = list()
        self._data_panels = dict()
        self._splitter_wnd = None

        self._statusbar = self.CreateStatusBar()
        field_widths = [-5, -1, -1, -1]
        self._statusbar.SetFieldsCount(len(field_widths), field_widths)

    # ---------------------------------------------------------------------------------------------
    def OnClose(self, event):
        # Save the mainframe's location.
        self.set_frame_layout()
        self._sash_position_config(False)
        self._save_configuration_file()
        event.Skip() # ok, since the default event handler does call Destroy()

    # ---------------------------------------------------------------------------------------------
    def get_frame_layout(self, section_name=None):
        '''
        :section_name   Section name in the configuration file for the frame. Can be a child frame.
        :return:        (was_minimized, (left, top), (width, height))
        :rtype:         tuple
        '''
        if not section_name:
            section_name = self._cfg_section_name

        config = self._cfg_parser
        layout = self._frame_layout
        if config.has_section(section_name):
            for key in layout:
                if config.has_option(section_name, key):
                    layout[key] = (config.getboolean(section_name, key) if key == 'minimized' else
                                   config.getint(section_name, key))
        return layout['minimized'], (layout['left'], layout['top']), (layout['width'],
                                                                      layout['height'])

    # ---------------------------------------------------------------------------------------------
    def set_frame_layout(self, section_name=None, screen_rect=None, minimized=None):
        '''
        Updates the configuration file's section for the window. 
        :section_name   Section name in the configuration file for the frame (can a a child frame).
        :screen_rect    wx.Rect object corresponding to the frame to set (can be a child frame).
        '''
        if not section_name:
            section_name = self._cfg_section_name
        if not screen_rect:
            screen_rect = self.GetScreenRect()
        if minimized is None:
            minimized = self.IsIconized()

        config = self._cfg_parser
        if not config.has_section(section_name):
            config.add_section(section_name)
        # self._frame_layout is used only for its keys. screen_rect contains information to write.
        for key in [k for k in self._frame_layout if k != 'minimized']:
            config.set(section_name, key, getattr(screen_rect, key))
        config.set(section_name, 'minimized', minimized)


    # ---------------------------------------------------------------------------------------------
    def _load_configuration_file(self):
        self._cfg_parser.read(self._cfg_filename)

    # ---------------------------------------------------------------------------------------------
    def _save_configuration_file(self):
        with open(self._cfg_filename, 'w') as w:
            self._cfg_parser.write(w)

    # ---------------------------------------------------------------------------------------------
    def write_status(self, display_text, pane_index=0):
        """Writes text to a pane in status bar."""
        if self._statusbar is None:
            logger.warning('No statusbar has been defined.')
        else:
            self._statusbar.SetStatusText(display_text, pane_index)

    # ---------------------------------------------------------------------------------------------
    def clear_status(self, pane_index=0):
        """Clears text from status bar."""
        if self._statusbar is None:
            logger.warning('No statusbar has been defined.')
        else:
            self._statusbar.SetStatusText('', pane_index)

    # ---------------------------------------------------------------------------------------------
    def get_statusbar_contents(self, pane_index=0):
        """Clears text from status bar."""
        if self._statusbar is None:
            logger.warning('No statusbar has been defined.')
            return ''
        else:
            return self._statusbar.GetStatusText(pane_index)

    # ---------------------------------------------------------------------------------------------
    def allocate_regions(self, n_data_pages=0, n_control_pages=0, controls_on_right=True):
        """
        Set up regions within the mainframe.
        
        data_pages       One of:
                         None  - have no data area.
                         True  - allocate space for a single scrolled window data area.
                         False - allocate space for a single data Panel.
                         List of booleans - allocate multiple tabbed data regions (or multiple
                         panes in a fourway splitter if style=='fws') with True and False per
                         region; each boolean has the the same meaning as above.
        :n_control_pages  Number of control pages (>= 0).
        """
        # If we have both a data area and a control area, then allocate a splitter window as a
        # parent of those areas. Otherwise, the mainframe is the parent.
        if (n_data_pages > 0) and (n_control_pages > 0):
            self._splitter_wnd = wx.SplitterWindow(self, style=wx.SP_3D)
            local_parent = self._splitter_wnd
        else:
            local_parent = self

        # Allocate data pages.
        if n_data_pages == 1:
            self._data_area = wx.Panel(local_parent)
            # For consistency with control page data:
            self._data_pages.append(self._data_area)
        elif n_data_pages > 1:
            self._data_area = wx.Notebook(local_parent)
            for i in range(n_data_pages):
                p = wx.Panel(self._data_area)
                self._data_pages.append(p)
                self._data_area.AddPage(p, 'Data page ' + str(i))

        self._allocate_control_pages(local_parent, n_control_pages)

        # Set up command handlers for when active page changes in a notebook.
        if (n_control_pages > 1) or (n_data_pages > 1):
            self._bind_notebook_functions()

        self._layout_regions(controls_on_right)

    # ---------------------------------------------------------------------------------------------
    def _bind_notebook_functions(self):
        # TODO
        pass

    # ---------------------------------------------------------------------------------------------
    def _allocate_control_pages(self, local_parent, n_control_pages):
        """
        Instantiate control area and pages.

        :param local_parent:     Parent window of the control area.
        :param n_control_pages:  Number of control pages required.
        """
        if n_control_pages == 1:
            self._control_area = wx.Panel(local_parent, style=wx.SUNKEN_BORDER)
            # To simplify validating control page data:
            self._control_pages.append(self._control_area)
        elif n_control_pages > 1:
            self._control_area = wx.Notebook(local_parent)
            # The Notebook is the parent of all the control pages in it.
            for i in range(n_control_pages):
                p = wx.Panel(self._control_area, style=wx.SUNKEN_BORDER)
                self._control_pages.append(p)
                self._control_area.AddPage(p, 'Ctrl page ' + str(i))

    # ---------------------------------------------------------------------------------------------
    def data_page(self, page_index=0):
        '''
        Returns the data page corresponding to page_index.

        If there are no data pages, then the returned object is None.
        If there is at least one data page, then the returned object is a wx.Panel or
        wx.ScrolledWindow.

        If the index is out of range, then the last data page is returned.

        :param int page_index:  Zero-based index in list of data pages
        '''
        page = self._data_area
        if len(self._data_pages) > 0:
            if (page_index >= len(self._data_pages)) or (page_index < 0):
                page_index = -1  # Take the last one
            page = self._data_pages[page_index]
        return page

    # ---------------------------------------------------------------------------------------------
    def control_page(self, page_index=0):
        '''
        Returns the page corresponding to page_index.

        :param int page_index:  Zero-based index in list of data pages
        '''
        page = self._control_area  # Could be None!
        if len(self._control_pages) > 0:
            if page_index >= len(self._control_pages):
                page_index = -1  # Take the last one
            page = self._control_pages[page_index]
        return page

    # ---------------------------------------------------------------------------------------------
    def configure_data_panels(self, data_panels):
        '''
        :param dict data_panels:  A map of wx.Panel-derived classes, keyed with the panel title
        '''
        if not isinstance(data_panels, dict):
            raise ValueError('Configure data panels with dictionary '
                             '{panel_name:panel_derived_class}')
        idx = 0
        for panel_name in data_panels:
            self._data_panels[panel_name] = data_panels[panel_name]
            self.data_page(idx).SetBackgroundColour('Black')
            self.set_data_page_tab_text(idx, panel_name)
            hbox = wx.BoxSizer(wx.HORIZONTAL)
            hbox.Add(self._data_panels[panel_name], 1, wx.EXPAND | wx.ALL, 10)
            self.data_page(idx).SetSizer(hbox) # NOT self._data_panels[panel_name].SetSizer()
            idx += 1


    # ---------------------------------------------------------------------------------------------
    def validate_control_pages(self, page_index=None):
        '''
        Validate the controls on a single control page, or all control pages.
        '''
        okay = True
        if page_index is None:
            for i, ctrl_page in enumerate(self._control_pages):
                if not transfer_vars_from_ui(ctrl_page):
                    wx.MessageBox(f'Error when validating control page '
                                f'{self._control_area.GetPageText(i)}')
                    okay = False
        else:
            if page_index >= len(self._control_pages):
                raise RuntimeWarning(f'Invalid index {page_index} passed to validate_control_pages().')
                okay = False
            else:
                if not transfer_vars_from_ui(self._control_pages[page_index]):
                    wx.MessageBox(f'Error when validating control page '
                                  f'{self._control_area.GetPageText(page_index)}')
                    okay = False
        return okay


    # ---------------------------------------------------------------------------------------------
    def set_active_data_page(self, page_index):
        """
        Sets the active page in a wx.Notebook to the specified index.

        :param page_index:    Index of the page in the wx.Notebook. If the index is out of range, then this
                             function does nothing.
        :param is_data_page:  True to select a data page; False to select a control page.
        """
        if page_index >= 0 and len(self._data_pages) > page_index:
                self._data_area.SetSelection(page_index)
        return

    # ---------------------------------------------------------------------------------------------
    def set_active_ctrl_page(self, page_index):
        """
        Sets the active page in a wx.Notebook to the specified index.

        :param page_index:    Index of the page in the wx.Notebook. If the index is out of range, then this
                             function does nothing.
        """
        if len(self._control_pages) > page_index:
            self._control_area.SetSelection(page_index)
        return

    # ---------------------------------------------------------------------------------------------
    def set_data_page_tab_text(self, index, text):
        '''
        '''
        n_data_pages = len(self._data_pages)
        if (self._data_area and 
            isinstance(self._data_area, wx.Notebook) and
            n_data_pages >= 1 and index < n_data_pages):
            self._data_area.SetPageText(index, text)
            
        # if (not self._data_area or not isinstance(self._data_area, wx.Notebook)):
        #     raise RuntimeWarning('Cannot apply tab text to object; object is not a wx.Notebook')
        # else:
        #     n_data_pages = len(self._data_pages)
        #     if (n_data_pages <= 1) or (index >= n_data_pages):
        #         raise RuntimeWarning(f'Page index {index} not valid '
        #                              f'when there are {n_data_pages} data pages')
        # self._data_area.SetPageText(index, text)

    # ---------------------------------------------------------------------------------------------
    def set_control_page_tab_text(self, index, text):
        '''
        '''
        n_control_pages = len(self._control_pages)
        if (n_control_pages == 0) or (index >= n_control_pages):
            raise RuntimeWarning(f'Page index {index} not valid when there are {n_control_pages} '
                                 f'control pages')
        else:
            self._control_area.SetPageText(index, text)
        return

    # ---------------------------------------------------------------------------------------------
    def _layout_regions(self, controls_on_right):
        """
        Lay out the regions within the main frame, splitting the application window with a 
        vertical sash. It is only necessary to call this function when a splitter window is being 
        used; without a splitter window, this function does nothing.

        :param bool controls_on_right:  True to have control panels on the right side of the main 
                                        application; False to have them on the left.
        """
        # One unexpected behavior:
        # SplitVertically() takes a sash position that is negative to specify the width of the
        # right pane, or positive to specify the width of the left pane.
        # However, a later call to GetSashPosition() always returns a positive value, corresponding
        # to the left pane.
        # Now that we're sometimes storing a GetSashPosition() in the configuration file, we have
        # to handle the fact that the value is always associated with the left pane, whether it's
        # the control or data area.
        sp = self._splitter_wnd
        if sp:
            if controls_on_right:
                left_side, right_side = self._data_area, self._control_area
            else:
                left_side, right_side = self._control_area, self._data_area
            # Return width will be -1 if there's no sash position in the configuration file.
            width = self._sash_position_config(True)
            if width < 0:                         # Calculate our own sash position
                if len(self._control_pages) > 1:
                    # Find the widest page to set the sash position.
                    width = max([pg.GetSize().x for pg in self._control_pages])
                else:
                    width = self.control_page(0).GetSize().x
                if controls_on_right:
                    # When the right panel has the controls, indicate its width with a -ve number.
                    width = -width
            sp.SplitVertically(left_side, right_side, width)
            # Set the sash gravity so that main frame size changes are all applied to the data
            # area. 0.0 means that only bottom/right panel grows; 1.0 means only top/left.
            sp.SetSashGravity(1.0 if controls_on_right else 0.0)

    # ---------------------------------------------------------------------------------------------
    def _sash_position_config(self, do_read):
        """Retrieves or saves the sash position with configparser."""
        
        value = -1 # indicates that the value is not in the configuration, or we're not using 
                   # a splitter.
        if self._splitter_wnd:
            if not do_read:
                # For writing, save the value. The value is always positive, and represents the
                # position relative to the left edge (window or client area?).
                value = self._splitter_wnd.GetSashPosition()
            value = config_value(do_read, 
                                 self._cfg_parser,
                                 self._cfg_section_name, 
                                 'Sash_position', value)
        return value

    # ---------------------------------------------------------------------------------------------
    def add_menu_bar(self, menu_data):
        """Add menus to the mainframe.

        Example menu_data:
        -----------------
            '&File' :
              {'label' : 'E&xit', 
              'help_string' : 'Terminate the program', 
              'handler' : self.OnExit, 
              'id' : wx.ID_EXIT}
            '&Run' :
             (
             {'label' : '&Option 1', 
             'help_string' : 'Run option 1', 
             'handler' : self.OnRunOption1},
             {'label' : '&Option 2', 
             'help_string' : 'Run option 2', 
             'handler' : self.OnRunOption2}
             )
            '&Help',
             {'label' : '&About', 
             'help_string' : 'Information about this program',
             'handler' : self.OnAbout, 
             'id' : wx.ID_ABOUT}
            }
        """
        # Create the menu bar. All the menus are added to this one.
        menu_bar = wx.MenuBar()
        for key in menu_data:
            if not isinstance(menu_data[key], tuple):
                menu_items = [menu_data[key]]
            else:
                menu_items = menu_data[key]
            menu_bar.Append(self._create_menu(menu_items), key)
        self.SetMenuBar(menu_bar)  # Add menu_bar to MainFrame.

    # ---------------------------------------------------------------------------------------------
    def _create_menu(self, menu_items):
        """Creates and returns a wx.Menu with specified items.

        :param list menu_items  List of menu dictionaries. Dict keys are:
            label = menu item label; use anything for separator
            help_string = help string displayed in the status bar
            handler = handler function associated with the menu item
            id = optional
                >=0 for pre-assigned ID, eg. wx.ID_ABOUT
                -1  to have the wx framework assign an ID - this is the default
                behavior when only three items are in the tuple.
            item_type = Default is ITEM_NORMAL.
                Options are ITEM_SEPARATOR, ITEM_NORMAL, ITEM_CHECK, or ITEM_RADIO.
            enabled = bool
        :rtype:     wx.Menu
        """
        menu = wx.Menu()
        for menu_dict in menu_items:
            if menu_dict.get('item_type') == wx.ITEM_SEPARATOR:
                menu.AppendSeparator()
            else:
                item_id = menu_dict.get('id', wx.NewId())
                item = menu.Append(item_id, 
                                   menu_dict['label'], 
                                   menu_dict['help_string'], 
                                   menu_dict.get('item_type', wx.ITEM_NORMAL))
                menu.Enable(item_id, menu_dict.get('enabled', True))
                self.Bind(wx.EVT_MENU, menu_dict['handler'], item)
        return menu


### TESTS ###
# why need to save self._cfg_section?
# what happens if allocate_regions called multiple times?
# zero control/data pages - exception thrown correctly while validating, etc.?
