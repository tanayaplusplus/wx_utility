import wx
import numpy as np
from image_and_bitmap import Image_and_bitmap
from image_and_bitmap import EventBitmap

class Image_display(wx.Panel):
    ''' Panel-derived class to display 2D image from ToF camera.'''
     
    # ---------------------------------------------------------------------------------------------
    def __init__(self, parent, status_bar):
        wx.Panel.__init__(self, parent)
        self._image = EventBitmap(self, wx.ID_ANY, wx.NullBitmap)
        self._img_data = Image_and_bitmap()

        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self._sizer.Add(self._image, proportion=0, flag=wx.ALL, border=10)
        self.SetSizer(self._sizer)
        self.Layout()

        #GetSize and GetClientSize don't seem to update on resizing. Something buggy; disabled.
        #self.Bind(wx.EVT_SIZE, self.On_resize)
        self._is_displayed = False

    # ---------------------------------------------------------------------------------------------
    def Draw(self, data):
        self._img_data.image_from_ndarray(data, image_type='wx')
        self._is_displayed = True
        self._img_data.zoom_image_to_fit(self)
        self._image.SetBitmap(self._img_data.bitmap)
        self.Refresh()


    # ---------------------------------------------------------------------------------------------
    def Clear_plot(self):
        self.Draw(np.zeros(self._img_data.ndarray.shape))

    # ---------------------------------------------------------------------------------------------
    def On_resize(self, event):
        event.Skip() # ????
        if self._is_displayed:
            iw = self._img_data.image.GetWidth()
            ih = self._img_data.image.GetHeight()
            i_aspect_ratio = float(iw) / float(ih)
            #print('Image aspect ratio (w / h):', i_aspect_ratio)
            pw, ph = self.GetClientSize()
            p_aspect_ratio = float(pw) / float(ph)
            #print('Panel aspect ratio (w / h):', p_aspect_ratio)
            #print('\tPanel width:', pw, 'height:', ph)

            if p_aspect_ratio > i_aspect_ratio:
                #print('Made wider: panel w/h > image w/h')
                zoomed_img = self._img_data.image.Scale(ph, i_aspect_ratio * ph)
            else:
                #print('Made longer: image w/h >= panel w/h')
                zoomed_img = self._img_data.image.Scale(pw / i_aspect_ratio, pw)

            self._image.SetBitmap(wx.Image.ConvertToBitmap(zoomed_img))
            self.Refresh()
            self.Layout()

