# example_app
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

from app_config import (initialize_global_parser, run_wx)
import argparse
import wx
from example_main_frame import Example_wx_mainframe

# =================================================================================================
class Example_wx_app(wx.App):

    # ---------------------------------------------------------------------------------------------
    def __init__(self, **kwargs):
        '''Saves the values in a dictionary of Dialog_elements to configparser.'''
        self.name = 'wx_demo'
        self._kwargs = kwargs
        super(Example_wx_app, self).__init__()
        return

    # ---------------------------------------------------------------------------------------------
    # This function is called automatically by the wxPython framework.
    def OnInit(self):
        """Called via wx.App's constructor."""
        self._mainframe = Example_wx_mainframe(self.name, **self._kwargs)
        self._mainframe.Show(True)
        self.SetTopWindow(self._mainframe)
        return True

# =================================================================================================
if __name__ == '__main__':
    # Either use the parser provided by initialize_global_parser, or create your own 
    p = initialize_global_parser('Example_wx_app')
    # add args to p if necessary/desired
    run_wx(p, Example_wx_app, 'Example_wx_app')