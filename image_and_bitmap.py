
# Copyright (c) PlusPlus Software, LLC, 2021

# $Id$

"""Image_and_bitmap class.

This class is for use in wxPython applications that use an image (wx.Image and/or PIL.Image) and a
bitmap representation (wx.Bitmap) of that image, or a zoomed version of that image.
It is also convenient for retrieving pixel data as a numpy ndarray.
It is useful for displaying tooltips on a bitmap, where the source image is to be queried for pixel
color components, etc.
"""

import logging
import wx

from PIL import Image as pil_image     # PIL is Pillow under Python 3.

try:
    from .image_data_formats import *
except ImportError as e:
    print(e)

logger = logging.getLogger(__name__)


# =================================================================================================
# Data members:
#   _filename:       Name of the file from which original_image was read.
#   _original_image: PIL.Image or wx.Image, usually initialized from a file, ndarray or another
#                    image.
#   _zoom:           Current zoom ratio.
#   _zoom_image:     Image stored when _zoom != 1.0.
#
# @@@ Edit the rest of this...
#   zoomed_image:   wx.Image with zoomed version of original_image.
##   image_edited: wx.Image with modified version of image_orig.
##   modified:     True if the original image has been edited.
# =================================================================================================
class Image_and_bitmap(object):
    """Image_and_bitmap class."""

    # ---------------------------------------------------------------------------------------------
    def __init__(self):
        """Constructor for Image_and_bitmap."""
        logger.debug('Image_and_bitmap.__init__()')
        self._filename = ''
        self._original_image = None
        self._zoom = 1.0
        self._zoom_image = None
        return

    # ---------------------------------------------------------------------------------------------
    @property
    def filename(self):
        """Returns the name of the image file opened.

        If the image in this class was created from a numpy array, then 'from ndarray' is returned
        as the filename.
        If the image in this class was assigned from another image, then 'from user image' is
        returned as the filename.
        If no image has been created yet, then an empty string is returned.
        """
        return self._filename

    # ---------------------------------------------------------------------------------------------
    @property
    def image(self):
        """Return reference to the image held by this class."""
        return self._original_image

    # ---------------------------------------------------------------------------------------------
    @property
    def image_dimensions(self):
        """Returns tuple with image width and image height, in pixels.

        If no image has been set up, both dimensions are returned as 0.
        """
        return ((0, 0) if (self._original_image is None) else
                width_and_height(self._original_image))

    # ---------------------------------------------------------------------------------------------
    @property
    def image_dimensions_text(self):
        """Returns text for the current image dimensions."""
        w, h = self.image_dimensions
        return '(' + str(w) + ' x ' + str(h) + ')'

    # ---------------------------------------------------------------------------------------------
    @property
    def zoom_text(self):
        """Returns text for the current zoom settings."""
        #separator = ':' if self._zoom.x >= self._zoom.y else '/'
        #return 'Zoom ' + str(self._zoom.x) + separator + str(self._zoom.y)
        return 'Zoom ' + str(self._zoom)

    # ---------------------------------------------------------------------------------------------
    @property
    def bitmap(self):
        """Returns bitmap of the current or zoomed image."""
        img = self._zoom_image if (self._zoom_image is not None) else self._original_image
        return None if (img is None) else wxBitmap_from_Image(img)

    # ---------------------------------------------------------------------------------------------
    def ndarray(self, conversion=None):
        """Returns numpy ndarray of image. More stuff about conversion..."""
        img = self._original_image
        return None if (img is None) else ndarray_from_Image(img, conversion)

    # ---------------------------------------------------------------------------------------------
    def open_image_file(self, filename, image_type='wx', flip_left_right=False,
                        flip_top_bottom=False):
        """Opens an image file and stores the contents."""
        # Delete any existing images.
        del self._original_image
        self._original_image = None

        self._filename = filename
        self._original_image = Image_from_file(filename, image_type)

        if flip_left_right or flip_top_bottom:
            self._original_image = flip_Image(self._original_image, flip_left_right,
                                                       flip_top_bottom)
        # Delete any existing zoom image, and reset zoom to 1.0.
        self.reset_zoom()
        return

    # ---------------------------------------------------------------------------------------------
    def image_from_ndarray(self, data_array, image_type=None):
        """Creates an image from the supplied numpy array."""
        # Delete any existing images.
        del self._original_image
        self._original_image = None

        # Set filename to indicate that image is from an ndarray.
        self._filename = 'from ndarray'
        self._original_image = Image_from_ndarray(data_array, image_type)

        # Delete any existing zoom image, and reset zoom to 1.0.
        self.reset_zoom()
        return

    # ---------------------------------------------------------------------------------------------
    def save_image_to_file(self, filename, save_zoomed_image=False):
        """Saves the current image to filename."""
        img = self._original_image
        if (save_zoomed_image) and (self._zoom_image is not None):
            img = self._zoom_image
        write_image_file(img, filename)
        return

    # ---------------------------------------------------------------------------------------------
    def reset_zoom(self):
        """Resets zoom settings to 1.0."""
        self._zoom = 1.0
        del self._zoom_image
        self._zoom_image = None
        return

    # ---------------------------------------------------------------------------------------------
    def zoom_image(self, zoom_ratio):
        """Create zoomed in or out image."""
        self._zoom = zoom_ratio
        del self._zoom_image
        self._zoom_image = zoom_image(self._original_image, zoom_ratio)
        #log_array_details('zoom_image', self._zoom_image)
        return

    # ---------------------------------------------------------------------------------------------
    def zoom_imageto_dims(self, w_new, h_new):
        """Create zoomed in or out image."""
        img_width, img_height = width_and_height(self._original_image)
        self._zoom = w_new / img_width
        del self._zoom_image
        self._zoom_image = zoom_image_to_dims(self._original_image, w_new, h_new)
        return

    # ---------------------------------------------------------------------------------------------
    def zoom_image_to_fit(self, window):
        """Zoom image to fit supplied window, without changing aspect ratio of image."""
        width, height = window.GetClientSize()
        img_width, img_height = width_and_height(self._original_image)
        # Casting to float is unnecessary for Python 3.
        self.zoom_image(min(width / img_width, height / img_height))
        return

    # ---------------------------------------------------------------------------------------------
    def unzoomed_coordinates(self, point):
        """Scales the point coordinates for a corresponding 1:1 image."""
        # Apply inverse of zoom. Return values are integers.
        #@@@ Round values later?
        unzoomed_x = int(point.x / self._zoom)
        unzoomed_y = int(point.y / self._zoom)
        return unzoomed_x, unzoomed_y

    # ---------------------------------------------------------------------------------------------
    def point_in_image(self, screen_point):
        """Returns true if the screen_point is within the displayed bitmap."""
        # Assumes only positive point coordinates, for now.
        x, y = self.unzoomed_coordinates(screen_point)
        width, height = self.image_dimensions
        return True if ((x < width) and (y < height)) else False

    # ---------------------------------------------------------------------------------------------
    def pixel(self, screen_point):
        """add stuff."""
        pix = None
        x, y = self.unzoomed_coordinates(screen_point)
        img = self._original_image
        if img is not None:
            pix = pixel_value(img, x, y)
        return pix

    # ---------------------------------------------------------------------------------------------
    def cross_section(self, screen_point, conversion=None):
        """Returns ndarrays for a horizontal and vertical cross-section.

        screen_point  wx.Point (typically from a mouse click) for the intersection of the two
                      cross-sections. The point can be from a zoomed image - zoom is removed here.
        conversion    'gray' to produce a grayscale plot. If no conversion is done on a color
                      image, then the returned array has two dimensions where the second is color
                      components and alpha.
        """
        #TODO: Handle general tuple for screen_point too.
        section_horiz = None
        section_vert  = None
        if self.point_in_image(screen_point):
            col, row = self.unzoomed_coordinates(screen_point)
            full_ndarray = self.ndarray(conversion)
            log_array_details('full_ndarray', full_ndarray)
            if full_ndarray.ndim == 2:
                section_horiz = full_ndarray[row, :].copy()
                section_vert  = full_ndarray[ :, col].copy()
            else:
                section_horiz = full_ndarray[row, :, :].copy()
                section_vert  = full_ndarray[ :, col, :].copy()
            del full_ndarray
            log_array_details('section_horiz', section_horiz)
            log_array_details('section_vert',  section_vert)

        return section_horiz, section_vert

    # ---------------------------------------------------------------------------------------------
    def assign_from_image(self, image):
        """Replace any existing image with the supplied one."""
        # Delete any existing images.
        del self._original_image
        self._original_image = None

        # Set filename to indicate that image is from a supplied image
        self._filename = 'from user image'
        self._original_image = image

        # Delete any existing zoom image, and reset zoom to 1.0.
        self.reset_zoom()
        return

    # ---------------------------------------------------------------------------------------------
    def PIL_image(self):
        #TODO: Fix/remove !
        """Returns a PIL image of the current image."""
        # Decide whether a copy is more appropriate!
        # Check it's not just a reference to an existing image! It is when type doesn't change.
        return PIL_image(self._original_image)


"""EventBitmap: A wxPython static bitmap class that passes on mouse events."""

# =============================================================================
class EventBitmap(wx.StaticBitmap):
    """ A static bitmap class that passes on mouse events.

    This class solves a problem with wx.StaticBitmap where mouse events in the
    region where the bitmap is displayed are not passed to the parent window,
    even with attempting to bind a function, and using Skip().
    Here, any mouse event is posted to the bitmap's parent, which seems to
    work fine.
    """
    # The choice of a StaticBitmap came from following Listing 12.1 in
    # "wxPython in Action" - sometime look into other bitmap options.

    # -------------------------------------------------------------------------
    def __init__(self, parent, bmp_id, bmp_label):
        """EventBitmap constructor."""
        # Renamed id to bmp_id to remove pylint warning.
        super(EventBitmap, self).__init__(parent, bmp_id, bmp_label)
        # Bind all mouse events to a function that posts the events to this
        # class's parent (the panel/scroll window).
        self.Bind(wx.EVT_MOUSE_EVENTS, self.OnMouseEvents)
        self.SetCursor(wx.Cursor(wx.CURSOR_CROSS))
        return

    # -------------------------------------------------------------------------
    def OnMouseEvents(self, event):
        """Post the mouse event to this class's parent."""
        wx.PostEvent(self.GetParent(), event)
        return
