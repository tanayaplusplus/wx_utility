import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
import matplotlib.patches as patches
from .custom_toolbar import Eight_bit_signal_toolbar as Toolbar

import os
import wx
import numpy as np

#--------------------------------------------------------------------------------------------------
class Multi_plot_panel(wx.Panel):
    """wx.Panel-derived class that contains a matplotlib x-y plot"""

    # ---------------------------------------------------------------------------------------------
    def __init__(self, parent, status_bar, rows=1, cols=2):
        wx.Panel.__init__(self, parent)
        self._active_plotting = True

        self._rows = rows
        self._cols = cols
        self._n_images = rows * cols
        self._figure = Figure()
        self._axes = list()
        self._right_axes = list()
        self._cbar = [None] * self._n_images
        for i in range(self._n_images):
            axs = self._figure.add_subplot(self._rows, self._cols, i+1)
            self._axes.append(axs)
            self._right_axes.append(axs.twinx())
        self.SetBackgroundColour('#ffffff')

        # Initialize the FigureCanvas, mapping the figure to the wx backend.
        self._canvas = FigureCanvas(self, wx.ID_ANY, self._figure)
        #self._gs = gridspec.GridSpec(self._rows, self._cols, left=0.05, bottom=0.05, right=0.95, top=0.95)

        self._toolbar = Toolbar(self._canvas, status_bar)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self._sizer.Add(self._canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self._sizer.Add(self._toolbar, 0 , wx.LEFT | wx.EXPAND)
        self.SetSizer(self._sizer)

    # ---------------------------------------------------------------------------------------------
    def Reset_subplots(self, rows, cols):
        '''
        Change the grid size.
        '''
        self._rows = rows
        self._cols = cols
        self._n_images = rows * cols
        self._axes.clear()
        self._right_axes.clear()
        self.Clear()
        for i in range(self._n_images):
            axs = self._figure.add_subplot(self._rows, self._cols, i+1)
            self._axes.append(axs)
            self._right_axes.append(axs.twinx())


    # ---------------------------------------------------------------------------------------------
    def Draw(self, i_subplot, data):
        '''
        Send plot of data to frame panel.
        :param data:    list of dicts ('plot_index', 'plot_type', 'title', 'x', 'y', 'y_side', 
                                       'y_color', 'format', 'markersize', 'data')
        '''
        if not isinstance(data, list):
            data = [data]
        
        self._axes[i_subplot].clear()
        if self._right_axes[i_subplot]:
            self._right_axes[i_subplot].clear()
        
        for i, d in enumerate(data):
            a = self._axes[i_subplot]
            plot_type = d.get('plot_type', 'plot')

            if plot_type == 'plot':
                if d.get('y_side','left') == 'left':
                    a.plot(d['x'], d['y'], d.get('format'), 
                        markersize=d.get('markersize', None))
                    a.tick_params(axis='both', which='major', labelsize=6)
                    a.tick_params(axis='y', colors=d.get('y_color', 'black'))
                else:
                    a_right = self._right_axes[i_subplot]
                    a_right.plot(d['x'], d['y'],
                                d.get('format'),
                                markersize=d.get('markersize', None))
                    a_right.tick_params(axis='both', which='major', labelsize=6)
                    a_right.tick_params(axis='y', colors=d.get('y_color', 'black'))
            elif plot_type == 'bw_image':
                img = d.get('data')
                if img is not None:
                    self._right_axes[i_subplot].axis('off')
                    im = a.imshow(img, cmap = 'gray', vmin=np.nanmin(img), vmax=np.nanmax(img))
                    a.set_title(d.get('title'))
            elif plot_type == 'rgb_image':
                img = d.get('data')
                if img is not None:
                    self._right_axes[i_subplot].axis('off')
                    im = a.imshow(img, vmin=np.nanmin(img), vmax=np.nanmax(img))
                    a.set_title(d.get('title'))
                    if self._cbar[i]:
                        self._cbar[i].remove()
                    self._cbar[i] = self._figure.colorbar(im, ax=self._right_axes[i_subplot])

        self._figure.tight_layout()
        # Note: could use wspace and hspace in subplots_adjust instead
        self._canvas.draw()

    # ---------------------------------------------------------------------------------------------
    def Clear(self):
        self._figure.clear()
                
    # ---------------------------------------------------------------------------------------------
    def Save_graph_as_png(self, filename=''):
        ''' Save graph to file '''
        self._figure.savefig(filename)

