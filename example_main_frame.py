# example_main_frame
# Copyright PlusPlus Software 2021. Please see License.txt for BSD3 license information.

from frame_config import Base_wx_frame

class Example_wx_mainframe(Base_wx_frame):

    # ---------------------------------------------------------------------------------------------
    def __init__(self, title, **kwargs):
        ''' '''
        super(Example_wx_mainframe, self).__init__(title, kwargs['cfg_file'])

        self.allocate_regions(n_data_pages=2, n_control_pages=2, controls_on_right=True)

