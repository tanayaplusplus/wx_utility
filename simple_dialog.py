
"""Implements a simple dialog."""

from __future__ import print_function  # So we can use print() as function.
import logging
import wx
from . import dialog_config as dlg_helper

logger = logging.getLogger(__name__)

# $Id$


# =============================================================================

class Simple_dialog(wx.Dialog):
    """Implements a simple dialog."""

    # ---------------------------------------------------------------------------------------------
    def __init__(self, dlg_data_items, dialog_title, box_title, ctrl_width=60,
                 item_range=None):
        """
        :param dlg_data_items:  An ordered dictionary of dialog elements to display
        :param dialog_title:    Text for the title bar of the dialog window
        :param box_title:       Text for the outline of the element container within the window
        :param ctrl_width:
        :param item_range:

        Lays out a row of controls and static text for each Dialog_element in dlg_data_items.
        If the Dialog_element requires a checkbox (the Dialog_element stores a bool value),
        then the row contains a single wx.CheckBox that spans all columns in the row.

        If the Dialog_element requires a text control (the Dialog_element stores a non-bool
        value), then the row comprises two or three columns. The first two
        columns are a label (prompt string), and the edit control.

        A unit string or a Browse button may be present in a third column.
        The column of unit strings/Browse buttons is only added when at least
        one Dialog_element requires a unit string or Browse button.

        These controls are all within a static box in a static box sizer.
        OK and Cancel buttons appear below the static box.
        """
        super(Simple_dialog, self).__init__(None, -1, dialog_title)

        # Create the OK and Cancel buttons with their preferred size.
        # We will use that size to set gaps between text controls, etc.
        btns, btn_size = dlg_helper.create_okay_and_cancel(self)
        # For a button height of 35 (pixels?), a gap of 15 seemed reasonable.
        gap = (btn_size.height * 3) // 7

        # Layout with sizers. Outermost sizer aligns its components vertically.
        outermost = wx.BoxSizer(wx.VERTICAL)

        self._dlg_data_items = dlg_data_items
        self._button_dict = dict()
        sbox_szr = dlg_helper.box_controls_and_labels(self, box_title,
            dlg_data_items, ctrl_width, gap, item_range,
            self._button_click_handler, self._button_dict, None)
        # Add these controls to the outermost sizer. The box is centered
        # horizontally, with a gap of 20 on all sides.
        outermost.Add(sbox_szr, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 20)

        # Add the OK and Cancel buttons.
        # Now centering horizontally.
        outermost.Add(btns, 0,
            wx.LEFT | wx.RIGHT | wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL, 20)

        # Finally, set the sizer in the dialog, and fit it.
        self.SetSizer(outermost)
        outermost.Fit(self)

        return

    # -------------------------------------------------------------------------
    def __del__(self):
        """Destructor for Simple_dialog."""
        logger.debug('In Simple_dialog::__del__()')
        return

    # -------------------------------------------------------------------------
    def _button_click_handler(self, event):
        """Handler for Browse button clicks."""
        #@@@ Add code to update DlgItems from the control windows, before
        # calling this next function? What happens if user has typed an invalid
        # directory or filename? How to handle that?
        dlg_helper.browse_and_update(event, self._dlg_data_items,
                                     self._button_dict,
                                     self.TransferDataToWindow)
        return


# -----------------------------------------------------------------------------
def run_modal_dialog(dialog_instance):
    """Shows modal dialog and returns True or False for OK or Cancel.

    This function calls Destroy() on the dialog instance after showing the
    dialog. It's very easy to forget to do this in code, so this function
    handles that for you.
    The easiest way to use this function is to create the dialog in the call to
    Run_modal_dialog(), for example:

        okay = Run_modal_dialog(Simple_dialog.Simple_dialog(data_items,
            dialog_title, box_title))

    Of course, this function is not restricted to Simple_dialog dialogs.
    Run_modal_dialog() is mainly useful for dialogs where parameters are passed
    back through a dictionary, since this function only returns a bool.
    """
    okay = (dialog_instance.ShowModal() == wx.ID_OK)
    dialog_instance.Destroy()
    return okay


# -------------------------------------------------------------------------------------------------
def show_simple_dialog(dlg_data_items, dialog_title, box_title, ctrl_width=60,
                       item_range=None, config_parser=None, section_name=None):
    """Shows a Simple_dialog and returns True or False for OK or Cancel.

    Parameters to this function are the same as for the Simple_dialog constructor, with the
    addition of the config_parser and section_name.
    """
    if config_parser is not None and section_name is not None:
        dlg_helper.config_read(config_parser, section_name, dlg_data_items, item_range)

    okay = run_modal_dialog(Simple_dialog(dlg_data_items, dialog_title, box_title,
                                                        ctrl_width, item_range))

    # If the user did not Cancel, then save the values back to the configuration.
    if okay and config_parser is not None and section_name is not None:
        dlg_helper.config_save(config_parser, section_name, dlg_data_items, item_range) 

    return okay
