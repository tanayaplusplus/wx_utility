
# Copyright (c) PlusPlus Software, LLC, 2021

# $Id$

"""image_data_formats module.

This module provides functions to convert between image data contained in wx.Image, PIL image,
wx.BitMap and numpy ndarray objects, and also data from OpenCV (numpy ndarrays).

Generally, this module handles images consisting of four common pixel formats:
* RGB  color images, where each color component of a pixel is represented by a single byte.
* RGBA color images, where an additional byte (beyond RGB) per pixel represents the alpha
 (transparency) of the pixel.
* Grayscale images, where each pixel value is one of 256 gray levels (internally, some images have
  three bytes per pixel, with each byte set to the same value).
* Grayscale images with two bytes (16 bits) of gray levels per pixel.

* TODO: Monochrome (bilevel) images.

wx.Images and PIL Images can both handle the first three types of image data; PIL images, but not
wx.Images, can handle 16-bit grayscale data.

 May add PNG with embedded data later.
 The code is based on Image_formats, which was based on HologramImage, with additions.
 @@@ Try to make this class functional under Python 2 and 3, and with wxPython
     Classic and Phoenix.
 @@@ Add some cv2 examples later.
 @@@ Add more 16-bit stuff too.
 This class provides several functions that use a common name for operations on PIL.Image or
 wx.Image instances.
 Using a Python module gives the functionality of a namespace.
 @@@ BGR to RGB.
 @@@ bitshift images.

Some caveats to be aware of:
* Some limitations of wx.Images:
  - wx.Images always save images as RGB or RGBA data. For grayscale images, the R, G and B
    components are all equal.
  - wx.Images always assume 8bits/pixel, and so cannot correctly represent 16-bit images.

* Some limitations of PIL images:
  - PIL Images can read and save 16-bit grayscale images, but not 16-bit RGB(A) images.
  - user is required to install the PIL package.

* Choosing a file type: PNG and TIFF both support lossless compression, so a round trip of creating
an image from array data, writing the image to a file, reading the image back from the file and
creating an array from the image contents yields the original data. Widely used formats such as JPG
typically do not support this round trip.  

* Superseded functions:
  - PIL_Image_from_ndarray() has been replaced by Image_from_ndarray().
  - PIL_Image_from_file() has been replaced by Image_from_file().
  - wxImage_from_ndarray() has been replaced by Image_from_ndarray().
  - wxImage_from_file() has been replaced by Image_from_file().
  - PIL_Image_from_wxImage() has been replaced by Image_from_Image().
  - wxImage_from_PIL_Image() has been replaced by Image_from_Image().
  - PIL_Image() has been replaced by Image_from_Image().
  - ndarray_from_PIL_Image() has been replaced by ndarray_from_Image().
  - ndarray_from_wxImage() has been replaced by ndarray_from_Image().
"""

from __future__ import print_function     # So we can use print() function.
import os.path                            # So we can extract filename extensions.
import logging
import numpy as np

import PIL                                # So we can get PIL.PILLOW_VERSION.
from PIL import TiffImagePlugin as TiffIP
from PIL import Image as pil_image        # PIL is Pillow under Python 3.

import wx                                 # wxPython framework.


logger = logging.getLogger(__name__)


# -------------------------------------------------------------------------------------------------
def Image_from_file(filename, image_type='wx', convert_to_grayscale=False):
    """Return a wx.Image or PIL Image, initialized with the contents of filename.

    Parameters
    ----------
    filename              Name of the file to open.
    image_type            'PIL' for a PIL image; anything else for a wx.Image (default).
    convert_to_grayscale   True to convert color images to grayscale (1 byte/pixel);
                          False to retain the file's image color properties.

    See 'How to choose a wx.Image or a PIL Image'.

    For a wx.Image, reading a file containing grayscale data is automatically converted
    into RGB data (with R=G=B). Unlike PIL Images, the main data stored in a wx.Image is always in
    RGB format. The alpha (transparency) data (if any) is stored in a separate array in the
    wx.Image.
    For a PIL Image, reading a file containing grayscale data is NOT automatically converted
    automatically into RGB data (R=G=B). (1 byte/pixel restriction?)
    Grayscale data is stored with all pixels having R=G=B components.
    @@@ Always use PIL when convert_to_grayscale == True.
    """
    #@@@ What about grayscale for 16-bit images?
    img = None
    if image_type == 'PIL':              # PIL Image?
        img = (pil_image.open(filename).convert('L') if convert_to_grayscale else
               pil_image.open(filename))
    else:                                # wx.Image
        img = (wx.Image(filename).ConvertToGreyscale() if convert_to_grayscale else
               wx.Image(filename))
    return img


# -------------------------------------------------------------------------------------------------
def Image_from_ndarray(data_array, image_type=None, conversion=None):
    """Creates a wx.Image or PIL Image from a numpy ndarray.

    Parameters
    ----------
    data_array  numpy ndarray.
    image_type  None  - select the most appropriate type automatically (define "appropriate"
                        later).
                'PIL' - use a PIL image
                'wx'  - use a wx.Image (which implies 1byte/color/pixel).
    conversion  'BGR_to_RGB' to swap the byte order from BGR to RGB for three-component color data
                (useful for converting opencv-generated data that are often in BGR order).
    """
    #@@@ Conversion parameter: eg. BGR to RGB?
    # This was the docstring for PIL_Image_from_ndarray().
    #"""Return a grayscale or RGB(A) PIL.Image from an ndarray.
    #Parameter
    #---------
    #data_array  2D ndarray with grayscale data (1, 2 or 4 bytes/pixel?), or
    #            a 3D array with RGB or RGBA data (1 byte/pixel).
    #"""
    # From wxImage_from_ndarray:
    #"""Return a RGB(A) wxImage from a grayscale, RGB or RGBA ndarray.

    #Parameter
    #---------
    #data_array  2D ndarray with grayscale (1 byte/pixel data), or a 3D array with RGB or RGBA data.

    #There is currently no option to add alpha data when the input data is grayscale.
    #(Use rgba_ndarray_from_ndarray() outside this function, and pass the result into this
    # function.)
    #"""
    # We're not handling 1D arrays (yet).
    logger.info('Image_from_ndarray()')
    img = None
    # Select a PIL Image if the ndarray has 16 bit data, which cannot be stored in a wx.Image.
    # Select a PIL Image if the ndarray contains grayscale values (smaller memory storage, and
    # smaller PNG image size, but possibly slower creating a bitmap).
    # Otherwise use a wx.Image.
    if image_type is None:
        image_type = 'PIL' if ((data_array.dtype == np.uint16) or (data_array.ndim == 2)) else 'wx'

    if image_type == 'PIL':                    # PIL Image?
        if (conversion == 'BGR_to_RGB') and (data_array.ndim == 3) and (data_array.shape[2] == 3):
            img = pil_image.fromarray(data_array[:, :, ::-1])  # BGR -> RGB
        else:
            img = pil_image.fromarray(data_array)
    else:                                      # wx.Image
        nrows, ncols = data_array.shape[:2]
        if data_array.dtype == np.uint16:
            logger.info('16-bit data!')
            #data_array = np.floor_divide(data_array, 256).astype(np.uint8)
            data_array = np.right_shift(data_array, 8).astype(np.uint8)
            log_array_details('data_array converted', data_array)
        # For grayscale data, make a 3D array, and copy the same grayscale values to each color
        # plane.
        if data_array.ndim == 2:
            rgb_array = rgb_ndarray_from_grayscale_ndarray(data_array)
        elif (data_array.ndim == 3) and (conversion == 'BGR_to_RGB'):
            rgb_array = data_array[:, :, ::-1]
        else:
            rgb_array = data_array
        log_array_details('rgb_array', rgb_array)
        # The wx.Image constructor has fourth argument for alpha values.
        # Check for the presence of alpha data by checking if the last dimension has a length of 4,
        # instead of 3. Use the last slice of the array as the alpha values.
        # From here to return img also works.
        #if rgb_array.shape[-1] < 4:
        #    img = wx.Image(ncols, nrows, rgb_array)
        #else:
        #    rgb = rgb_array[:,:,:3].copy()
        #    transparency = rgb_array[:,:,3].copy()
        #    log_array_details('rgb', rgb)
        #    log_array_details('transparency', transparency)
        #    # This next line works.
        #    #img = wx.Image(ncols, nrows, rgb, transparency)
        #    img = wx.Image(ncols, nrows, rgb_array[:,:,:3],
        #                   rgb_array[:,:,3].copy())
        #return img
        #print('rgb_array[:,:,:3].shape', rgb_array[:,:,:3].shape)
        #if rgb_array.shape[-1] == 4:
        #    print('rgb_array[:,:,3].shape', rgb_array[:,:,3].shape)
        img = (wx.Image(ncols, nrows, rgb_array) if (rgb_array.shape[2] < 4) else
               wx.Image(ncols, nrows, rgb_array[:, :, :3].copy(), rgb_array[:, :, 3].copy()))
    return img


# -------------------------------------------------------------------------------------------------
def Image_from_Image(image, output_image_type='wx'):
    """Return required image type from converted input image."""
    # Could add some checks on image type, to decide whether conversion is necessary. Makes it
    # simpler for caller code - no need for explicit check on image type. Copy or reference -
    # reference makes sense when image type is not changing.
    return Image_from_ndarray(ndarray_from_Image(image), output_image_type)


# -------------------------------------------------------------------------------------------------
def is_wxImage(image):
    """Return True if image is a wx.Image instance."""
    # type(PIL.Image) varies depending on what file type was opened (PNG, TIFF, etc.), for example:
    # <class 'PIL.PngImagePlugin.PngImageFile'> and
    # <class 'PIL.TiffImagePlugin.TiffImageFile'>, in two instances I checked.
    # wx.Image always returns the same type. So, check for a wx.Image to distinguish between
    # wx.Image and PIL.Image.
    logger.debug(type(image))
    return isinstance(image, wx.Image)


# -------------------------------------------------------------------------------------------------
def wxBitmap_from_Image(image):
    """Return a wx.Bitmap from either a wx.Image or a PIL.Image."""
    if not is_wxImage(image):
        image = Image_from_Image(image)  # Create wx.Image from PIL.Image
    # Do some timing tests for PIL and wx Images.
    # Also, look into direct conversion from ndarray.
    return wx.Bitmap(image)


# -------------------------------------------------------------------------------------------------
def flip_Image(image, flip_left_right, flip_top_bottom):
    """Flip a wx.Image or a PIL.Image horizontally and/or vertically.

    flip_left_right:  True to flip horizontally.
    flip_top_bottom:  True to flip vertically.
    """
    # If both flip_left_right and flip_top_bottom are False, then the original image will be
    # returned.
    # The order of these two flips is not important.
    if is_wxImage(image):
        if flip_left_right:
            image = image.Mirror(True)
        if flip_top_bottom:
            image = image.Mirror(False)
    else:
        if flip_left_right:
            image = image.transpose(PIL.Image.FLIP_LEFT_RIGHT)
        if flip_top_bottom:
            image = image.transpose(PIL.Image.FLIP_TOP_BOTTOM)
    return image


# -------------------------------------------------------------------------------------------------
def ndarray_from_Image(input_image, conversion=None):
    """Return a numpy ndarray from a PIL.Image or wx.Image.

    The input image is not modified by this function.

    Parameters
    ----------
    input_image  PIL.Image or wx.Image that is _not_ empty.
    conversion   Controls what format the returned ndarray will have:
                    None   - make no explicit conversions... more stuff
                    'RGB'  - ndarray will have three dimensions with RGB  data.
                    'RGBA' - ndarray will have three dimensions with RGBA data.
                    'gray' - grayscale 2D ndarray.
    """
    logger.info('ndarray_from_Image()')
    if is_wxImage(input_image):
        logger.info('Using wx.Image')
        # wx.Images always have at least RGB pixel components.
        # Alpha values are stored in a separate array in the image.
        # Use the 'RGBA' routine if no conversion is required and the image
        # has alpha values; otherwise 'RGB' is the same as no conversion.
        if conversion is None:
            conversion = 'RGBA' if (input_image.HasAlpha()) else 'RGB'

        # GetData() returns a 1D array of RGB values, per pixel.
        img_arr = np.asarray(input_image.GetData())
        logging.info('wx.Image.GetData() returns a 1D array, shape %s', img_arr.shape)
        ncols, nrows = input_image.GetSize()
        colors = 3
        if conversion == 'RGB':
            # Reshape the ndarray to reflect the image dimensions.
            arr = img_arr.reshape((nrows, ncols, colors),)
        elif conversion == 'RGBA':
            # Make the data format the same as for a PIL color image.
            arr = np.empty((nrows, ncols, colors+1), dtype=img_arr.dtype)
            arr[:, :, 0:colors] = img_arr.reshape((nrows, ncols, colors),)
            if input_image.HasAlpha():
                arr[:, :, colors] = np.asarray(
                    input_image.GetAlpha()).reshape(nrows, ncols)
            else:
                # Fake some alpha data if the image doesn't have any.
                arr[:, :, colors] = 255  # Set completely opaque
        elif conversion == 'gray':
            # Convert to greyscale keeps RGB components, but they are all
            # equal. Select every third one to get a simple grayscale
            # array.
            arr2 = np.asarray(input_image.ConvertToGreyscale().GetData())
            arr = arr2[::3].copy().reshape((nrows, ncols),)

        # The numpy array always has three dimensions, even if the image was
        #created from a file with only grayscale data (8bits/pixel).
        #The length of the third dimension is either 3 (for RGB) or 4 (for RGBA
        #data present in the image, and with append_alpha == True).
        #"""
        #print('wx.Image.GetData() returns a 1D array, shape', arr.shape)
        #ncolors = arr.shape[0] // (ncols * nrows)  # Always 3.
        #if (input_image.HasAlpha()) and (append_alpha is True):
        #    # Make the data format the same as for a PIL color image.
        #    nd_arr = np.empty((nrows, ncols, ncolors+1), dtype=arr.dtype)
        #    nd_arr[:, :, 0:3] = arr.reshape((nrows, ncols, ncolors),)
        #    nd_arr[:, :, 3] = np.asarray(
        #        input_image.GetAlpha()).reshape(nrows, ncols)
        #    ncolors += 1
        #else:
        #    nd_arr = arr.reshape((nrows, ncols, ncolors),)
    else:                                    # PIL.Image
        logger.info('Using PIL.Image')
        #If the PIL image was created from color data, then the numpy array has
        #three dimensions; for grayscale data, the array has two dimensions.
        #The length of the third dimension (for color images) is either
        #3 (for RGB) or 4 (for RGBA).
        #arr.dtype is uint8 for 8 bit/color/pixel, or uint16 for 16-bit TIFF.
        # Assuming a non-empty image?
        if conversion is None:
            arr = np.array(input_image)
        elif (conversion == 'RGB') or (conversion == 'RGBA'):
            #@@@ Check that the input_image is not modified!
            arr = np.array(input_image.convert(conversion))
        elif conversion == 'gray':
            #@@@ Check that the input_image is not modified!
            arr = np.array(input_image.convert('L'))
    logger.info('   returning ndarray, shape: %s  dtype: %s', arr.shape, arr.dtype)
    return arr


# -------------------------------------------------------------------------------------------------
def rgb_ndarray_from_grayscale_ndarray(gray_array):
    """Replicates grayscale data into ndarray with RGB components."""
    n_rows, n_cols = gray_array.shape
    rgb_array = np.empty((n_rows, n_cols, 3), dtype=gray_array.dtype)
    for color in range(3):  # For R, G, B
        rgb_array[:, :, color] = gray_array
    return rgb_array


# -------------------------------------------------------------------------------------------------
def rgba_ndarray_from_ndarray(input_ndarray, alpha_data):
    """Returns RGBA ndarray from grayscale or RGB ndarray input.

    Parameters
    ----------
    input_array  2D ndarray with grayscale (1 byte/pixel data), or a 3D array with RGB data.
    alpha_data   Single value for all pixels' alpha, or 2D ndarray with individual pixels' values.
    """
    n_rows, n_cols = input_ndarray.shape[:2]
    nplanes = 4
    rgba_array = np.empty((n_rows, n_cols, nplanes), dtype=input_ndarray.dtype)
    if input_ndarray.ndim == 3:  # Input data is RGB?
        rgba_array[:, :, :3] = input_ndarray
    else:                        # Input data is grayscale.
        for color in range(3):   # For R, G, B
            rgba_array[:, :, color] = input_ndarray
    # The alpha data must be a single integer, or a 2D array of per-pixel values.
    rgba_array[:, :, 3] = alpha_data
    return rgba_array


# -------------------------------------------------------------------------------------------------
def pixel_value(image, x, y):
    """Returns components as a RGB(A) tuple, or single grayscale value.

    If the supplied (x, y) point is not in the image, None is returned.
    (x, y) values are 0-based.
    """
    pix = None
    width, height = width_and_height(image)
    if (x < width) and (y < height) and (x >= 0) and (y >= 0):
        if is_wxImage(image):
            pix = (image.GetRed(x, y), image.GetGreen(x, y), image.GetBlue(x, y))
            if image.HasAlpha():
                pix = (pix[0], pix[1], pix[2], image.GetAlpha(x, y))
        else:                           # PIL Image
            #@@@ Does the returned tuple contain Alpha when present?
            pix = image.getpixel((x, y))
    return pix


# -------------------------------------------------------------------------------------------------
def width_and_height(image):
    """Returns wx.Image or PIL.Image width and height, as a tuple.

    If the image is empty, then (0, 0) is returned.
    """
    # This functionality is used enough to warrant a separate function.
    # Old comments from HologramImage - probably no longer relevant.
    # The constructor for this class initializes self.original_image:
    # self.original_image = wx.Image() if wx_version.is_phoenix() else \
    #                                     wx.EmptyImage()
    # Without loading some subsequent image, a call to
    # get_image_dimensions(), when it just calls
    # self.original_image.GetSize() causes a crash:
    # In HologramImage.get_image_dimensions()
    #<class 'wx._core.Image'>
    #Traceback (most recent call last):
    #  File "C:\FFT_and_GS\Python_gs\GS_mainframe.py", line 389, in on_run_gs
    #    wx_size = self.target_image.get_image_dimensions()
    #  File "C:\Source_A\Python\wx_utility\HologramImage.py", line 188, in get_image_dimensions
    #    return self.original_image.GetSize()
    #wx._core.wxAssertionError: C++ assertion "IsOk()" failed at ..\..\src\common\image.cpp(1739)
    # in wxImage::GetWidth(): invalid image
    # Digging in futher, it seems that when IsOk() returns False (which it does for this case),
    # GetSize() fails, instead of returning the "obvious" dimensions of (0, 0). (Maybe there's a
    # distinction between a (0, 0)-sized image and "empty"?)
    # So, I'm checking IsOk() directly, and returning (0, 0) for that case.
    # This behavior is definitely in Phoenix, and I think in classic too.
    #if self.modified:
    #    return self.image_edited.GetSize()
    width, height = 0, 0
    if is_wxImage(image):
        if image.IsOk():
            width, height = image.GetSize()
    else:
        width, height = image.size
    return width, height


# -------------------------------------------------------------------------------------------------
def write_image_file(image, filename):
    """Writes image file from the PIL.Image or wx.Image.

    File format is deduced from the filename extension.
    """
    if is_wxImage(image):
        image.SaveFile(filename)
    else:                                  # PIL Image
        if not 'tif' in os.path.splitext(filename)[1].lower():
            image.save(filename)
        else:                     # TIFF image
            # Without an IFD structure, TIFF files written from PIL Image are not readable by
            # FreeImage (though IrfanView, PIL.Image, Windows, etc.) can read them.
            # FreeImage complained that there were 65535 samples per pixel, whereas everything else
            # seems to assume 1 for the case where it's not explicitly provided. So, add the
            # tiffinfo for this special case.
            # Try and make a dictionary to provide the number of samples/pixel for each supported
            # mode.
            # From: https://pillow.readthedocs.io/en/4.1.x/handbook/concepts.html#concept-modes
            # "The mode of an image defines the type and depth of a pixel in the image. The current
            # release supports the following standard modes:"
            spp = {
                '1': 1,      # 1 (1-bit pixels, black and white, stored with one pixel per byte)
                'L': 1,      # L (8-bit pixels, black and white)
                'P': 1,      # P (8-bit pixels, mapped to any other mode using a color palette)
                'RGB': 3,    # RGB (3x8-bit pixels, true color)
                'RGBA': 4,   # RGBA (4x8-bit pixels, true color with transparency mask)
                'CMYK': 4,   # CMYK (4x8-bit pixels, color separation)
                'YCbCr': 3,  # YCbCr (3x8-bit pixels, color video format)  Note that this refers to
                             # the JPEG, and not the ITU-R BT.2020, standard.
                'LAB': 3,    # LAB (3x8-bit pixels, the L*a*b color space)
                'HSV': 3,    # HSV (3x8-bit pixels, Hue, Saturation, Value color space)
                'I': 1,      # I (32-bit signed integer pixels)
                'F': 1,      # F (32-bit floating point pixels)
                # PIL also provides limited support for a few special modes,
                # including LA (L with alpha), RGBX (true color with padding) and RGBa (true color
                # with premultiplied alpha). (My guesses for the number of samples/pixel).
                'LA': 2,
                'RGBX': 4,
                'RGBa': 3
                }
            # Image mode can be I;16 (for example), so just keep the I.
            # (If 16-bit data is provided, then the TIFF ends up with 16-bit values.)
            i_mode = image.mode.split(';')[0]
            logger.info('i_mode %s', i_mode)
            if i_mode not in spp:
                raise ValueError
            else:
                # This next line worked when testing this script directly,
                # but gave error when called from Okapy.
                #ifd = PIL.TiffImagePlugin.ImageFileDirectory_v2()
                ifd = TiffIP.ImageFileDirectory_v2()
                # Tag 277 is SamplesPerPixel (TIFF6.pdf, page 24).
                ifd[277] = spp[i_mode]
                #img.save(fname, resolution=72, resolution_unit='inch')
                image.save(filename, tiffinfo=ifd)
    return


# -------------------------------------------------------------------------------------------------
def zoom_image(original_image, zoom_ratio):
    """Return zoomed in (zoom_ratio > 1) or zoomed out (zoom_ratio < 1).

    If the zoom_ratio is 0, or is 'close' to 1.0, then the original image
    is returned.
    """
    zoomed_image = original_image
    if ((zoom_ratio > 0.0) and (zoom_ratio < 0.995)) or (zoom_ratio > 1.005):
        w, h = width_and_height(original_image)
        w = int(w * zoom_ratio + 0.5)
        h = int(h * zoom_ratio + 0.5)
        if is_wxImage(original_image):
            zoomed_image = original_image.Scale(w, h)
        else:
            # Use simplest resampling filter, for now.
            zoomed_image = original_image.resize((w, h), PIL.Image.NEAREST)
    return zoomed_image


# -------------------------------------------------------------------------------------------------
def zoom_image_to_dims(original_image, w_new, h_new):
    """Return scaled image."""
    zoomed_image = original_image
    if is_wxImage(original_image):
        zoomed_image = original_image.Scale(w_new, h_new)
    else:
        # Use simplest resampling filter, for now.
        zoomed_image = original_image.resize((w_new, h_new), PIL.Image.NEAREST)
    return zoomed_image


# -------------------------------------------------------------------------------------------------
def log_image_details(name, image):
    """Log some properties of a PIL.Image or wx.Image. For debugging."""
    logger.info('%s is a %s Image.', name, 'wx' if is_wxImage(image) else 'PIL')
    width, height = width_and_height(image)
    logger.info('%s dimensions:  width=%d  height=%d', name, width, height)
    if is_wxImage(image):
        #@@@ Add this!
        pass
    else:
        logger.info('%s mode: %s', name, image.mode)
        #@ Test this!
        logger.info('%s info: %s', name, image.info)
    return


# -------------------------------------------------------------------------------------------------
def log_array_details(name, arr):
    """Log some properties of a numpy ndarray. Principally for debugging."""
    logger.info('%s:  shape: %s  size: %s (bytes)  dtype: %s', name, arr.shape, arr.size,
                arr.dtype)
    return


# =================================================================================================

# Functions from here to the end are intended to exercise the above functions, and illustrate how
# to use the functions.


# -------------------------------------------------------------------------------------------------
def _create_example_rgb_ndarray(n_rows=48, n_cols=64):
    """Return an RGB ndarray (1 byte/color/pixel) for a simple image with four quadrants.

    Parameters:
    -----------
    n_rows      Number of rows in the ndarray - this will correspond to the height of an image
                created from the returned ndarray.
    n_cols      Number of columns in the ndarray - this will correspond to the width of an image
                created from the returned ndarray.

    No alpha (transparency) data is present in the returned nd_array.
    """
    # Allocate a 3D numpy ndarray. The red color component is in plane 0, the green in plane 1, and
    # the blue in plane 2. Each color component is an unsigned 8-bit integer.
    arr = np.empty((n_rows, n_cols, 3), dtype=np.uint8)
    # Set all pixels to have color components: R=50, G=75, B=100
    for plane, value in enumerate([50, 75, 100]):
        arr[:, :, plane] = value
    # Add 100 to each color component in the upper right and lower left quadrants, just so we can
    # later verify that we are reading from the correct pixel locations (not with row and column
    # interchanged).
    center_row, center_col = (n_rows // 2), (n_cols // 2)
    arr[: center_row, center_col :, :] += 100   # upper right quadrant
    arr[center_row :, : center_col, :] += 100   # lower left quadrant
    return arr


# -------------------------------------------------------------------------------------------------
def _create_16bit_grayscale_ndarray():
    """Return a 16-bit/pixel grayscale ndarray, with all (65536) possible pixel values.

    Returned ndarray is 256x256 pixels, with pixel values increasing from left to right along each
    row. Upper left pixel is 0, and lower right pixel is 65535.
    """
    return np.arange(65536, dtype=np.uint16).reshape(256, 256)


# -------------------------------------------------------------------------------------------------
def _verify_file_reads(file_list):
    """Demonstrate reading files into images, and writing out some image and ndarray attributes."""
    logger.info('\n\nVerifying image reads:')
    # No conversion to grayscale in this loop.
    for f in file_list:
        logger.info('\nFile %s:', f)
        for i in range(2):               # For wx.Image, then PIL Image
            img = Image_from_file(f, 'PIL' if (i) else 'wx')
            log_image_details(f, img)
            arr = ndarray_from_Image(img)
            # Show that the shape of the ndarray is reported as rows, cols, i.e. the reverse of the
            # Image, which reports x (width), and y (height).
            rows, cols = arr.shape[0 : 2]
            logger.info('rows: %d   cols: %d', rows, cols)
            logger.info('numpy.ndarray:  shape: %s  size: %s (bytes)  dtype: %s', arr.shape,
                        arr.size, arr.dtype)
            if i == 0:
                first = arr.copy()
            else:
                same = np.array_equal(first, arr)
                logger.info('ndarrays are %s', 'the same' if (same) else 'different!')
            # Report the color components for two different pixels, from the image and the ndarray.
            # Pixel (x, y) coordinates: (0, 0) and (40, 12).
            # Using (0, 0) shows that it is the (left, top) corner of the image (not the left,
            # bottom corner).
            # (40, 12) is in the upper right quadrant; if the coordinates are reversed, the
            # corresponding pixel would be in the lower left quadrant.
            for x, y in ((0, 0), (40, 12)):
                logger.info('x = %d  y = %d', x, y)
                logger.info('%s', pixel_value(img, x, y))  # Directly from image.
                logger.info('%s', arr[y, x] if (arr.ndim < 3) else arr[y, x, :])
    return


